﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core2Mvc_ViewComp.Models;
using Microsoft.AspNetCore.Mvc;

namespace Core2Mvc_ViewComp.Controllers
{
    public class HomeController : Controller
    {
        private ICategoryRepository categoryRepository;
        private IProductRepository productRepository;
        public HomeController(ICategoryRepository _categoryRepository, IProductRepository _productRepository)
        {
            categoryRepository = _categoryRepository;
            productRepository = _productRepository;
        }

        public IActionResult Index()
        {
            return View(productRepository.Products);
        }
    }
}