﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core2Mvc_ViewComp.Models
{
    public interface IProductRepository
    {
        IEnumerable<Product> Products { get; }
        void AddCategory(Product entity);
    }
}
