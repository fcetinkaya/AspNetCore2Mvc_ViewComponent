﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core2Mvc_ViewComp.Models
{
    public class Category : IDisposable
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
