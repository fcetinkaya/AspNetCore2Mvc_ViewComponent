﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core2Mvc_ViewComp.Models
{
    public class ProductRepository : IProductRepository
    {
        private List<Product> _products = new List<Product>()
        {
            new Product(){ProductId=1,ProductName="Samsung S7",Price=2456,IsApproved=true,CategoryId=1 },
            new Product(){ProductId=2,ProductName="Samsung S8",Price=3450,IsApproved=true,CategoryId=1 },
            new Product(){ProductId=3,ProductName="Samsung S9",Price=4406,IsApproved=false,CategoryId=1 },
            new Product(){ProductId=4,ProductName="Sony Laptop Voi 2451",Price=12400,IsApproved=true,CategoryId=2 },
            new Product(){ProductId=5,ProductName="Samsung Laptop Core 2 4564",Price=8300,IsApproved=true,CategoryId=2 },
            new Product(){ProductId=6,ProductName="Lenova Laptop L4587 ",Price=6450,IsApproved=true,CategoryId=2 },
            new Product(){ProductId=7,ProductName="Samsung Tablet S3",Price=2400,IsApproved=true,CategoryId=3 },
            new Product(){ProductId=8,ProductName="Apple Air Pro",Price=7450,IsApproved=true,CategoryId=3 },
            new Product(){ProductId=9,ProductName="Lenovo Tablet L4",Price=4400,IsApproved=true,CategoryId=3 }
        };


        public IEnumerable<Product> Products => _products;

        public void AddCategory(Product entity)
        {
            _products.Add(entity);
        }
    }
}
