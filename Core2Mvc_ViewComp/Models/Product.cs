﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core2Mvc_ViewComp.Models
{
    public class Product : IDisposable
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public double Price { get; set; }
        public bool IsApproved { get; set; }

        public int CategoryId { get; set; }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
