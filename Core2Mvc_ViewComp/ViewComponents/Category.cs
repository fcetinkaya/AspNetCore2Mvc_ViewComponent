﻿using Core2Mvc_ViewComp.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core2Mvc_ViewComp.ViewComponents
{
    public class Category:ViewComponent
    {
        private readonly ICategoryRepository _category;

        public Category(ICategoryRepository category)
        {
            _category = category;
        }

        public IViewComponentResult Invoke()
        {
            return View(_category.Categories);
        }


    }
}
