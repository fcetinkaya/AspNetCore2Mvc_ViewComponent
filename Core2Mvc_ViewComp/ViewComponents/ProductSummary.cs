﻿using Core2Mvc_ViewComp.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core2Mvc_ViewComp.ViewComponents
{
    public class ProductSummary : ViewComponent
    {
        private IProductRepository productRepository;

        public ProductSummary(IProductRepository _productRepository)
        {
            productRepository = _productRepository;
        }

        public string Invoke()
        {
            return $"{productRepository.Products.Count()} ürün toplam fiyatı {productRepository.Products.Sum(i => i.Price)}";
        }


    }
}
